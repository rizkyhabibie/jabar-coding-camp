console.log('====== Soal Nomor Satu ======')
{
    const hitungluas = () => {
        let panjang = 5;
        let lebar = 6;
        let luas = panjang * lebar;
        console.log(luas)
    }
    hitungluas()
}

console.log('====== Soal Nomor Dua =======')
{
    const nama = () => {
        const fullname = 'William Imoh'
        console.log(fullname)
    }
    nama()
}

console.log('====== Soal Nomor Tiga ======')
{
    let namapeserta = {
        firstName: "Muhammad",
        lastName: "Iqbal Mubarok",
        address: "Jalan Ranamanyar",
        hobby: "playing football",
    };
    const {firstName, lastName, address, hobby} = namapeserta
    console.log(firstName, lastName + ',', address, hobby)
}

console.log('====== Soal Nomor Empat =====')
{
    const west = ["Will", "Chris", "Sam", "Holly"];
    const east = ["Gill", "Brian", "Noel", "Maggie"];
    
    let produce = [...west, ...east];
    
    
    console.log(produce);
}

console.log('====== Soal Nomor Lima ======')
{
    const planet = 'earth'
    const view = 'glass'

    const hasil = `${planet} ${view}`
    console.log(hasil)
}