console.log('===============Soal Nomor Satu===============')
// Soal nomor 1
{
    // variabel pertama "saya sangat senang hari ini"
    var satu = 'saya';
    var dua = 'sangat';
    var tiga = 'senang';
    var empat = 'hari';
    var lima = 'ini';
    // variabel kedua "belajar javascript itu keren"
    var s1 = 'belajar';
    var s2 = 'javascript';
    var s3 = 'itu';
    var s4 = 'keren';
    
    // output soal nomor 1
    var result = `${satu} ${tiga} ${s1} ${s2}`
    console.log(result) 
    
}


console.log('===============Soal Nomor Dua================')


// Soal nomor 2
{
 var angka1 = 10;
 var angka2 = 2;
 var angka3 = 4;
 var angka4 = 6;


//  output soal nomor 2
 var jumlah = angka1 + angka2 * angka3 + angka4;
 console.log(jumlah);
    
}

console.log('===============Soal Nomor Tiga================')

{
    // soal nomor 3
    var sentence = "wah javascript itu keren sekali"; 
    var katapertama = sentence[0] + sentence[1] + sentence[2];
    var katakedua = sentence[4] + sentence[5] +sentence[6] + sentence[7] + sentence[8] + sentence[9] + sentence[10] + sentence[11] + sentence[12] +sentence[13];
    var kataketiga = sentence[14] + sentence[15] + sentence[16] + sentence[17];
    var katakeempat = sentence[18] + sentence[19] + sentence[20] + sentence[21] + sentence[22] + sentence[23];
    var katakelima = sentence[24] + sentence[24] + sentence[25] + sentence[26] + sentence[27] + sentence[28] + sentence[29] + sentence[30];
    
    // output soal nomor 3
    console.log('katapertama :' + katapertama);
    console.log('katakedua :' + katakedua);
    console.log('kataketiga :' + kataketiga);
    console.log('katakeempat :' + katakeempat);
    console.log('katakelima :' + katakelima);
}